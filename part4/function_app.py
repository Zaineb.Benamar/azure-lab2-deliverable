import azure.functions as func
import logging
import math
app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    result = 0.0

    for i in range(N):
        x_i = lower + i * delta_x
        area = abs(math.sin(x_i)) * delta_x
        result += area

    return result

@app.route(route="numericalintegral")
def numercialintegral(req: func.HttpRequest) -> func.HttpResponse:

    N_values = [10, 100, 1000, 10000, 100000, 1000000]
    results = {}
    lower = float(req.params.get('lower', 0.0))
    upper = float(req.params.get('upper', math.pi))
    for N in N_values:
        integral_result = numerical_integration(float(lower), upper, N)
        results[N] = integral_result

    return func.HttpResponse(
            f"{results}",
            status_code=200
    )








