import azure.functions as func
import azure.durable_functions as df
from azure.storage.blob import BlobServiceClient

myApp = df.DFApp(http_auth_level=func.AuthLevel.ANONYMOUS)

# An HTTP-Triggered Function with a Durable Functions Client binding
@myApp.route(route="orchestrators/{functionName}")
@myApp.durable_client_input(client_name="client")
async def http_start(req: func.HttpRequest, client):
    function_name = req.route_params.get('functionName')
    instance_id = await client.start_new(function_name,None)
    response = client.create_check_status_response(req, instance_id)
    return response


@myApp.orchestration_trigger(context_name="context")
def master_orchestrator(context):
    # Step 0: Get Input Data
    input = yield context.call_activity("GetInputDataFn", "")

    # Fan-out: Map phase
    mapTasks = [context.call_activity("mapper_function", {key: value}) for key, value in input.items()]
    output = yield context.task_all(mapTasks)

    # Fan-in: Shuffle phase
    shuffled = yield context.call_activity("shuffler_function", output)

    # Fan-out: Reduce phase
    reduceTasks = [context.call_activity("reducer_function", {key: values}) for key, values in shuffled.items()]

    # Fan-in: Aggregate the reduced results
    finalResult = yield context.task_all(reduceTasks)

    return finalResult

@myApp.activity_trigger(input_name="start")
def GetInputDataFn(start):
    # Replace these with your storage account connection string and container name
    connection_string = "DefaultEndpointsProtocol=https;AccountName=benamar;AccountKey=mYTthjUSi9ASwawZBoOj5SOsAyJvox19S7zTJULcUxNcWAOVP0mWo2w/MJuotm3ZwaTLZIiix4YG+AStWfRyyw==;EndpointSuffix=core.windows.net"
    container_name = "mapreduce-blob"

    blob_service_client = BlobServiceClient.from_connection_string(connection_string)
    container_client = blob_service_client.get_container_client(container_name)

    input = {}

    # List blobs in the container
    blobs = container_client.list_blobs()
    offset=0
    for blob in blobs:
        blob_client = container_client.get_blob_client(blob.name)
        blob_data = blob_client.download_blob().readall()

        # Process blob_data and build input
        lines = blob_data.decode("utf-8").splitlines()
        for line in (lines):
            input.update({str(offset): line})
            offset+=1

    return input

@myApp.activity_trigger(input_name="input")
def mapper_function(input):
    for key, value in input.items():
        words = value.split()
        output = [(word, 1) for word in words]
    return output

@myApp.activity_trigger(input_name="output")
def shuffler_function(output):
    shuffled = {}
    for lines in output:
        for key, value in lines:
            if key not in shuffled:
                shuffled[key] = []
            shuffled[key].append(value)
    return shuffled

@myApp.activity_trigger(input_name="shuffled")
def reducer_function(shuffled):
    result = {}
    for key, values in shuffled.items():
        total_count = sum(values)
        result[key] = total_count
    return result







