from flask import Flask, jsonify
import math

app = Flask(__name__)

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    result = 0.0

    for i in range(N):
        x_i = lower + i * delta_x
        area = abs(math.sin(x_i)) * delta_x
        result += area

    return result

@app.route('/numericalintegralservice/<int:lower>/<float:upper>')
def calculate_integral(lower, upper):
    N_values = [10, 100, 1000, 10000, 100000, 1000000]
    results = {}

    for N in N_values:
        integral_result = numerical_integration(float(lower), upper, N)
        results[N] = integral_result

    return jsonify(results)

if __name__ == '__main__':
    app.run(debug=True)
