from locust import HttpUser, task, between

class NumericalIntegrationUser(HttpUser):
    wait_time = between(1, 2)  # Random wait time between requests
    host = "https://numericalintegrall.azurewebsites.net/api"
    #host = "https://integralnum.azurewebsites.net"
    #host = "http://157.56.177.136:80"
    @task
    def calculate_integral(self):
        #response = self.client.get('/numericalintegralservice/0/3.14159')
        response = self.client.get('/numericalintegral?lower=0&upper=3.14159')
